__author__ = 'd22admin'

import sys
import json
import datetime

def setType(data, typeValue):
    data["a"] = typeValue

def setDate(data, timeStamp):
    data["startedAtTime"] = timeStamp

def addKeyListToObj(data, usedSoftwareKeyName):
    data[usedSoftwareKeyName] = []

def setLshParam(data,lshParam,  usedSoftwareKeyName):
    tempObj = {}
    tempObj["a"] = "usedSoftwareKeyName"
    tempObj["comment"] = "Spark MinHash/LSH"
    tempObj["L13_used_parameters"] = lshParam
    data[usedSoftwareKeyName].append(tempObj)

def setFilterParam(data,lshParam,  usedSoftwareKeyName):
    tempObj = {}
    tempObj["a"] = "usedSoftwareKeyName"
    tempObj["comment"] = "filterLshResults.py"
    tempObj["L13_used_parameters"] = lshParam
    data[usedSoftwareKeyName].append(tempObj)

def setIntegrationParam(data, usedSoftwareKeyName):
    tempObj = {}
    tempObj["a"] = "usedSoftwareKeyName"
    tempObj["comment"] = "integrateLshLinks.py"
    data[usedSoftwareKeyName].append(tempObj)

def addSourceObj(data, usedSourceKeyName, source):
    tempObj = {}
    tempObj["a"] = "D1_Digital_Object"
    tempObj["uri"] = source
    data[usedSourceKeyName].append(tempObj)

def parse_args():
    global lshParameters
    global filterParameters
    global sourceObj1
    global sourceObj2
    global timeStamp
    global output

    for arg_idx, arg in enumerate(sys.argv):
        if arg == "--lshParameters":
            lshParameters = sys.argv[arg_idx+1]
            continue
        if arg == "--filterParameters":
            filterParameters = sys.argv[arg_idx+1]
            continue
        if arg == "--sourceObj1":
            sourceObj1 = sys.argv[arg_idx+1]
            continue
        if arg == "--sourceObj2":
            sourceObj2 = sys.argv[arg_idx+1]
            continue
        if arg == "--timeStamp":
            timeStamp = sys.argv[arg_idx+1]
            continue
        if arg == "--output":
            output = sys.argv[arg_idx+1]
            continue

lshParameters = None
filterParameters = None
sourceObj1 = None
sourceObj2 = None
output = None
timeStamp= None
typeValue = "D10_Software_Execution"
usedSoftwareKeyName = "L23_used_software_or_firmware"
usedSourceKeyName = "L2_used_as_source"
parse_args()

provData = {}
setType(provData, typeValue)
setDate(provData, datetime.datetime.now().isoformat())
addKeyListToObj(provData, usedSoftwareKeyName)
setIntegrationParam(provData,usedSoftwareKeyName)
if lshParameters is not None and lshParameters != "": setLshParam(provData,lshParameters,usedSoftwareKeyName)
if filterParameters is not None and filterParameters != "": setFilterParam(provData,filterParameters,usedSoftwareKeyName)
addKeyListToObj(provData, usedSourceKeyName)
addSourceObj(provData, usedSourceKeyName, sourceObj1)
if sourceObj2 is not None and sourceObj2 != "": addSourceObj(provData, usedSourceKeyName, sourceObj2)
provJson = json.dumps(provData, indent=4, sort_keys=True)
fd = open(output, 'w')
fd.write(str(provJson))
fd.close()