#!/bin/sh

if [ -z "$1" ]
  then
    echo "No query file supplied."
fi
count=0
for filename in $1/*;do 
	count=$(($count  +  1))
	cat $filename >> $1/tempLSHmerge
done
echo $count Files were concatinated
mv $1/tempLSHmerge $1/$2
#sort -k1,1 -k3,3r $1/$2.tsv > $1/tempSorted

#clusterFilePathRanked=$tempDirName/$tempClusterDirName/$inputBaseFileName'_with_'$inputTargetFileName'_'$inputNumHashes'H_'$inputNumItemsInBand'I_'$lshUserPrefix'_cluster_ranked.tsv'
#python ../../11_python/mrrRankLSHclusters.py --input $1/tempSorted --output $1/$2_ranked.tsv
#rm $1/tempSorted